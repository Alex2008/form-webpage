document.getElementById('registrationForm').addEventListener('submit', function(event) {
    event.preventDefault();

    // Basic validation
    var name = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    var dob = document.getElementById('dob').value;
    var gender = document.getElementById('gender').value;

    if (name && email && password && dob && gender) {
        // All fields are filled
        document.getElementById('successMessage').classList.remove('hidden');
    } else {
        alert('Please fill all fields correctly.');
    }
});
